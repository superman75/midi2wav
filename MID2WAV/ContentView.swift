//
//  ContentView.swift
//  MID2WAV
//
//  Created by bakhtiyor on 11/21/20.
//

import SwiftUI

struct ContentView: View {
  let timidity = TimidityAPI()
  let appSupportURL = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first
  @State var isDocumentPickerPresented: Bool = false
  @State var isExportingDocument = false
  @State var documentUrl: URL? = nil
  @State var isProgress: Bool = false
  @State var isCompleted: Bool = false
  @State var isError: Bool = false
  @State var error: String = "Error"
  @State var url: URL? = nil
  
  
  var body: some View {
    HStack {
      VStack {
        Text("Select MIDI File To Convert")
          .padding()
          .font(Font.headline)
        Button(action: {
          self.isDocumentPickerPresented.toggle()
        }, label: { Text("Browse") })
        .frame(alignment: .center)
        .padding()
        .sheet(isPresented: self.$isDocumentPickerPresented, content: {
          DocumentPickerViewController { url in
            self.documentUrl = url
            self.isCompleted = false
            self.isError = false
            self.isProgress = true
            
            print(url)
            
            DispatchQueue.main.async {
              if let documentUrl = self.documentUrl {
                self.url = timidity.convert(withMidiFile: documentUrl)
                self.isProgress = false
                self.isCompleted = true
              }
            }
          }
        })
        if (self.isProgress) {
          ProgressView("Converting…").padding()
        }
        if (self.isCompleted && self.url != nil) {
          Button("Export WAV") { self.isExportingDocument = true }
            .background(DocumentInteractionController($isExportingDocument,
                                                      url: self.url!))
        }
        if (self.isError) {
          Text(error).foregroundColor(.red)
        }
        
        
        Spacer()
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
