//
//  MID2WAVApp.swift
//  MID2WAV
//
//  Created by bakhtiyor on 11/21/20.
//

import SwiftUI

@main
struct MID2WAVApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
