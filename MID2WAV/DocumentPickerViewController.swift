//
//  DocumentPickerViewController.swift
//  MID2WAV
//
//  Created by bakhtiyor on 11/22/20.
//

import SwiftUI
import MobileCoreServices

struct DocumentPickerViewController: UIViewControllerRepresentable {
  var callback: (URL) -> ()
  
  func makeCoordinator() -> Coordinator {
    return Coordinator(documentController: self)
  }
  
  func updateUIViewController(_ uiViewController: UIDocumentPickerViewController,
                              context: UIViewControllerRepresentableContext<DocumentPickerViewController>) {
  }
  
  func makeUIViewController(context: Context) -> UIDocumentPickerViewController {
    let controller = UIDocumentPickerViewController(documentTypes: [String(kUTTypeMIDIAudio)], in: .open)
    controller.delegate = context.coordinator
    return controller
  }
  
  class Coordinator: NSObject, UIDocumentPickerDelegate {
    var documentController: DocumentPickerViewController
    
    init(documentController: DocumentPickerViewController) {
      self.documentController = documentController
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
      guard let url = urls.first, url.startAccessingSecurityScopedResource() else { return }
      defer { url.stopAccessingSecurityScopedResource() }
      documentController.callback(urls[0])
    }
  }
}
