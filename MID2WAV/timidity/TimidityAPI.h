//
//  TimiditiAPI.h
//  MID2WAV
//
//  Created by bakhtiyor on 11/21/20.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TimidityAPI : NSObject
- (instancetype)init;
- (NSURL*)convertWithMidiFile:(NSURL*)midiFileURL;
+ (NSString *)configFilePath;
+ (long)version;
@end

NS_ASSUME_NONNULL_END
