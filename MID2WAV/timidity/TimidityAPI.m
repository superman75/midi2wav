//
//  TimiditiAPI.m
//  MID2WAV
//
//  Created by bakhtiyor on 11/21/20.
//

#import "TimidityAPI.h"
#include "timidity.h"
#include "wav_header_utils.h"


@interface TimidityAPI ()
- (BOOL)isConfigFileExists;
- (void)createConfigFile;
+ (void)copyFolder:(NSString *)path destination:(NSString *)destination;
@end

@implementation TimidityAPI
- (instancetype)init {
  self = [super init];
  if (self != NULL) {
    if (![self isConfigFileExists]) {
      [self createConfigFile];
    }
  }
  return self;
}

- (NSURL*)convertWithMidiFile:(NSURL *)midiFileURL {
  NSError* error;
  
  NSString *appSupportApp =
      NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                          NSUserDomainMask, true)
          .firstObject;
  NSURL* tmpFileURL = [NSURL fileURLWithPath:[appSupportApp stringByAppendingPathComponent:[NSUUID UUID].UUIDString]];

  BOOL isAcccessing = [midiFileURL startAccessingSecurityScopedResource];
  BOOL r = [[NSFileManager defaultManager] copyItemAtURL:midiFileURL toURL:tmpFileURL error:&error];
  if (isAcccessing) [midiFileURL stopAccessingSecurityScopedResource];
  
  if (!r) {
    NSLog(@"Error: %@", error.description);
    return nil;
  }
  NSString* midiFile = tmpFileURL.absoluteURL.path;
  
  NSURL* wavURL = [NSURL fileURLWithPath:[appSupportApp stringByAppendingPathComponent:[[NSUUID UUID].UUIDString stringByAppendingString:@".wav"]]];
  NSString* wavFile = wavURL.absoluteURL.path;
  
  int result = mid_init([TimidityAPI.configFilePath UTF8String]);
  if (result != 0) {
    NSLog(@"Couldn't init libtimidity, result code: %d", result);
    mid_exit();
    return nil;
  }
  
  int rate = 44100;
  int bits = 16;
  int channels = 2;
  int volume = 100;
  FILE * output;
  MidIStream *stream;
  MidSongOptions options;
  MidSong *song;
  sint8 buffer[4096];
  size_t bytes_read;
  stream = mid_istream_open_file([midiFile UTF8String]);
  if (stream == NULL) {
    NSLog(@"Could not open file: %@", midiFile);
    mid_exit();
    [[NSFileManager defaultManager] removeItemAtURL:tmpFileURL error:&error];
    return nil;
  }
  
  options.rate = rate;
  options.format = (bits == 16)? MID_AUDIO_S16LSB : MID_AUDIO_U8;
  options.channels = channels;
  options.buffer_size = sizeof (buffer) / (bits * channels / 8);
  song = mid_song_load (stream, &options);
  mid_istream_close (stream);

  if (song == NULL) {
    NSLog(@"Invalid MIDI file %@", midiFile);
    mid_exit();
    [[NSFileManager defaultManager] removeItemAtURL:tmpFileURL error:&error];
    return nil;
  }
  
  if (!(output = fopen ([wavFile UTF8String], "wb"))) {
    NSLog(@"Could not open output file %@", wavFile);
    mid_song_free (song);
    mid_exit ();
    [[NSFileManager defaultManager] removeItemAtURL:tmpFileURL error:&error];
    return nil;
  }
  
  write_prelim_header(output, channels, bits, rate);
  mid_song_set_volume (song, volume);
  mid_song_start (song);
  int written = 0;
  while ((bytes_read = mid_song_read_wave(song, buffer, sizeof (buffer)))) {
    fwrite(buffer, bytes_read, 1, output);
    written += bytes_read;
  }
  rewrite_header(output, written);

  mid_song_free (song);
  mid_exit ();
  fclose(output);
  [[NSFileManager defaultManager] removeItemAtURL:tmpFileURL error:&error];

  return wavURL;
}

- (BOOL)isConfigFileExists {
  return [[NSFileManager defaultManager]
      fileExistsAtPath:[TimidityAPI configFilePath]];
}

- (void)createConfigFile {
  NSError *error = nil;
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSBundle *mainBundle = [NSBundle mainBundle];

  NSString *appSupportDir =
      NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                          NSUserDomainMask, true)
          .firstObject;

  if (![fileManager fileExistsAtPath:appSupportDir isDirectory:NULL]) {
    if (![fileManager createDirectoryAtPath:appSupportDir
                withIntermediateDirectories:YES
                                 attributes:nil
                                      error:&error]) {
      NSLog(@"%@", error.localizedDescription);
    }
  }

  NSString *freepatsCfgPath = [mainBundle pathForResource:@"freepats"
                                                   ofType:@"cfg"];
  NSString *contentFreepatsConfig =
      [NSString stringWithContentsOfFile:freepatsCfgPath
                                encoding:NSUTF8StringEncoding
                                   error:&error];

  NSString *path = [mainBundle pathForResource:@"timidity" ofType:@"cfg"];
  NSString *content = [NSString stringWithContentsOfFile:path
                                                encoding:NSUTF8StringEncoding
                                                   error:nil];
  content = [content stringByReplacingOccurrencesOfString:@"!FREEPATS_ROOT_DIR!"
                                               withString:appSupportDir];
  content = [content stringByAppendingString:contentFreepatsConfig];
  BOOL res = [content writeToFile:[TimidityAPI configFilePath]
                       atomically:TRUE
                         encoding:NSUTF8StringEncoding
                            error:&error];
  if (!res) {
    NSLog(@"Error: %@", error.description);
  }
  
  NSString *freepatsDrumDir = [[mainBundle pathForResource:@"025_Snare_Roll" ofType:@"pat" inDirectory:@"Drum_000"] stringByDeletingLastPathComponent];
  NSString *freepatsToneDir = [[mainBundle pathForResource:@"000_Acoustic_Grand_Piano" ofType:@"pat" inDirectory:@"Tone_000"] stringByDeletingLastPathComponent];
  [TimidityAPI copyFolder:freepatsDrumDir destination:[appSupportDir stringByAppendingPathComponent:@"Drum_000"]];
  [TimidityAPI copyFolder:freepatsToneDir destination:[appSupportDir stringByAppendingPathComponent:@"Tone_000"]];
}

+ (void)copyFolder:(NSString *)sourcePath
       destination:(NSString *)destinationPath {
  NSFileManager *fileManager = [NSFileManager defaultManager];
  NSError *error;

  if (![fileManager fileExistsAtPath:destinationPath]) {
    [fileManager createDirectoryAtPath:destinationPath
           withIntermediateDirectories:NO
                            attributes:nil
                                 error:&error];
  } else {
    NSLog(@"Directory exists! %@", destinationPath);
  }

  NSArray *fileList = [fileManager contentsOfDirectoryAtPath:sourcePath
                                                       error:&error];
  for (NSString *s in fileList) {
    NSString *newFilePath = [destinationPath stringByAppendingPathComponent:s];
    NSString *oldFilePath = [sourcePath stringByAppendingPathComponent:s];
    if (![fileManager fileExistsAtPath:newFilePath]) {
      [fileManager copyItemAtPath:oldFilePath toPath:newFilePath error:&error];
    } else {
      NSLog(@"File exists: %@", newFilePath);
    }
  }
}

+ (NSString *)configFilePath {
  NSString *appSupportDir =
      NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                          NSUserDomainMask, true)
          .firstObject;
  return [appSupportDir stringByAppendingPathComponent:@"timidity.cfg"];
}

+ (long)version {
  return mid_get_version();
}
@end
