//
//  wav_header_utils.h
//  MID2WAV
//
//  Created by bakhtiyor on 11/22/20.
//

#ifndef wav_header_utils_h
#define wav_header_utils_h

#include <stdio.h>

int write_prelim_header(FILE * out, int channels, int bits, int samplerate);
int rewrite_header (FILE * out, unsigned int written);

#endif /* wav_header_utils_h */
